package ru.t1.lazareva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.request.DataYamlLoadFasterXmlRequest;

public class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-yaml";

    @NotNull
    private static final String DESCRIPTION = "Load data from yaml file.";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(getToken());
        getDomainEndpoint().loadDataYamlFasterXml(request);
    }

}