package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.lazareva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.lazareva.tm.api.endpoint.IUserEndpoint;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.dto.request.*;
import ru.t1.lazareva.tm.dto.response.*;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.exception.AbstractException;
import ru.t1.lazareva.tm.marker.SoapCategory;
import ru.t1.lazareva.tm.model.Project;
import ru.t1.lazareva.tm.service.PropertyService;

@Category(SoapCategory.class)
public final class ProjectEndpointTest {

    @Nullable
    public final static String ADMIN_LOGIN = "admin";
    @Nullable
    public final static String ADMIN_PASSWORD = "admin";
    @Nullable
    public final static String USER_LOGIN = "test1";
    @Nullable
    public final static String USER_PASSWORD = "test1";
    @Nullable
    public final static String USER_EMAIL = "USER_EMAIL";
    @Nullable
    public final static String PROJECT_NAME = "PROJECT_NAME";
    @Nullable
    public final static String PROJECT_DESC = "PROJECT_DESC";
    @Nullable
    public final static String PROJECT_NAME_NEW = "PROJECT_NAME_NEW";
    @Nullable
    public final static String PROJECT_DESC_NEW = "PROJECT_DESC_NEW";
    @Nullable
    public final static Status PROJECT_STATUS = Status.IN_PROGRESS;
    @Nullable
    public final static Sort PROJECT_SORT = Sort.BY_CREATED;
    @NotNull
    private final static IPropertyService PROPERTY_SERVICE = new PropertyService();
    @NotNull
    private final static IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);
    @NotNull
    private final static IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);
    @NotNull
    private final static IProjectEndpoint ENDPOINT = IProjectEndpoint.newInstance(PROPERTY_SERVICE);
    @Nullable
    private static String ADMIN_TOKEN;
    @Nullable
    private static String USER_TOKEN;
    @Nullable
    private static Project USER_PROJECT;
    @Nullable
    private static String USER_PROJECT_ID;

    @BeforeClass
    public static void before() throws AbstractException {
        @NotNull final UserLoginRequest loginAdminRequest = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final UserLoginResponse loginAdminResponse = AUTH_ENDPOINT.login(loginAdminRequest);
        ADMIN_TOKEN = loginAdminResponse.getToken();

        @NotNull final UserRegistryRequest userRegistryRequest = new UserRegistryRequest(USER_LOGIN, USER_PASSWORD, USER_EMAIL);
        USER_ENDPOINT.registryUser(userRegistryRequest);

        @NotNull final UserLoginRequest loginUserRequest = new UserLoginRequest(USER_LOGIN, USER_PASSWORD);
        @NotNull final UserLoginResponse loginUserResponse = AUTH_ENDPOINT.login(loginUserRequest);
        USER_TOKEN = loginUserResponse.getToken();

        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(USER_TOKEN);
        projectCreateRequest.setName(PROJECT_NAME);
        projectCreateRequest.setDescription(PROJECT_DESC);
        @NotNull final ProjectCreateResponse projectCreateResponse = ENDPOINT.createProject(projectCreateRequest);
        USER_PROJECT = projectCreateResponse.getProject();
        USER_PROJECT_ID = USER_PROJECT.getId();
    }

    @AfterClass
    public static void after() throws AbstractException {
        ENDPOINT.clearProject(new ProjectClearRequest(USER_TOKEN));
        USER_PROJECT = null;
        USER_PROJECT_ID = null;

        @NotNull final UserLogoutRequest logoutUserRequest = new UserLogoutRequest(USER_TOKEN);
        AUTH_ENDPOINT.logout(logoutUserRequest);
        USER_TOKEN = null;

        @NotNull final UserRemoveRequest removeUserRequest = new UserRemoveRequest(ADMIN_TOKEN);
        removeUserRequest.setLogin(USER_LOGIN);
        USER_ENDPOINT.removeUser(removeUserRequest);

        @NotNull final UserLogoutRequest logoutAdminRequest = new UserLogoutRequest(ADMIN_TOKEN);
        AUTH_ENDPOINT.logout(logoutAdminRequest);
        ADMIN_TOKEN = null;
    }

    @Test
    public void getProjectById() throws AbstractException {
        @NotNull final ProjectShowByIdRequest projectShowByIdRequest = new ProjectShowByIdRequest(USER_TOKEN);
        projectShowByIdRequest.setId(USER_PROJECT_ID);
        @Nullable final ProjectShowByIdResponse response = ENDPOINT.showProjectById(projectShowByIdRequest);
        response.setId(USER_PROJECT_ID);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject());
    }

    @Test
    public void getAllProjects() throws AbstractException {
        @NotNull final ProjectListResponse responseNoSort = ENDPOINT.listProject(new ProjectListRequest(USER_TOKEN));
        Assert.assertNotNull(responseNoSort);
        Assert.assertNotNull(responseNoSort.getProjects());

        @NotNull final ProjectListResponse responseWithSort = ENDPOINT.listProject(new ProjectListRequest(USER_TOKEN, PROJECT_SORT));
        Assert.assertNotNull(responseWithSort);
        Assert.assertNotNull(responseWithSort.getProjects());
    }


    @Test
    public void clearProjects() throws AbstractException {
        @NotNull final ProjectClearResponse response = ENDPOINT.clearProject(new ProjectClearRequest(USER_TOKEN));
        Assert.assertNotNull(response);

        @NotNull final ProjectListResponse responseGetAll = ENDPOINT.listProject(new ProjectListRequest(USER_TOKEN, PROJECT_SORT));
        Assert.assertNull(responseGetAll.getProjects());

        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(USER_TOKEN);
        projectCreateRequest.setName(PROJECT_NAME);
        projectCreateRequest.setDescription(PROJECT_DESC);
        @NotNull final ProjectCreateResponse projectCreateResponse = ENDPOINT.createProject(projectCreateRequest);
        USER_PROJECT = projectCreateResponse.getProject();
        USER_PROJECT_ID = USER_PROJECT.getId();
    }

    @Test
    public void removeProjectById() throws AbstractException {
        @NotNull final ProjectRemoveByIdRequest projectRemoveByIdRequest = new ProjectRemoveByIdRequest(USER_TOKEN);
        projectRemoveByIdRequest.setId(USER_PROJECT_ID);
        @Nullable final ProjectRemoveByIdResponse response = ENDPOINT.removeProjectById(projectRemoveByIdRequest);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject());

        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(USER_TOKEN);
        projectCreateRequest.setName(PROJECT_NAME);
        projectCreateRequest.setDescription(PROJECT_DESC);
        @NotNull final ProjectCreateResponse projectCreateResponse = ENDPOINT.createProject(projectCreateRequest);
        USER_PROJECT = projectCreateResponse.getProject();
        USER_PROJECT_ID = USER_PROJECT.getId();
    }

    @Test
    public void createProject() throws AbstractException {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(USER_TOKEN);
        projectCreateRequest.setName(PROJECT_NAME);
        projectCreateRequest.setDescription(PROJECT_DESC);
        @NotNull final ProjectCreateResponse projectCreateResponse = ENDPOINT.createProject(projectCreateRequest);
        Assert.assertNotNull(projectCreateResponse);
        @Nullable final Project project = projectCreateResponse.getProject();
        Assert.assertNotNull(project);
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(USER_TOKEN);
        request.setId(project.getId());
        Assert.assertNotNull(ENDPOINT.showProjectById(request));
    }

    @Test
    public void changeProjectStatusById() throws AbstractException {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(USER_TOKEN);
        request.setId(USER_PROJECT_ID);
        request.setStatus(PROJECT_STATUS);
        @Nullable final ProjectChangeStatusByIdResponse response = ENDPOINT.changeProjectStatusById(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject().getStatus());
        Assert.assertEquals(PROJECT_STATUS, response.getProject().getStatus());
    }

    @Test
    public void updateProjectById() throws AbstractException {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(USER_TOKEN);
        request.setId(USER_PROJECT_ID);
        request.setName(PROJECT_NAME_NEW);
        request.setDescription(PROJECT_DESC_NEW);
        @Nullable final ProjectUpdateByIdResponse response = ENDPOINT.updateProjectById(request);
        Assert.assertNotNull(response);
        Assert.assertEquals(PROJECT_NAME_NEW, response.getProject().getName());
        Assert.assertEquals(PROJECT_DESC_NEW, response.getProject().getDescription());
    }

    @Test
    public void startProjectById() throws AbstractException {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(USER_TOKEN);
        request.setId(USER_PROJECT_ID);
        @Nullable final ProjectStartByIdResponse response = ENDPOINT.startProjectById(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject().getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void completeProjectById() throws AbstractException {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(USER_TOKEN);
        request.setId(USER_PROJECT_ID);
        @Nullable final ProjectCompleteByIdResponse response = ENDPOINT.completeProjectById(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject().getStatus());
        Assert.assertEquals(Status.COMPLETED, response.getProject().getStatus());
    }

}