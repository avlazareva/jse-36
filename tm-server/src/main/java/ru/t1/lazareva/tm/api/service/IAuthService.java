package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.model.Session;
import ru.t1.lazareva.tm.model.User;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    @NotNull
    User check(@Nullable String login, @Nullable String password);

    @NotNull
    Session validateToken(@Nullable String token);

    void invalidate(@Nullable Session session);

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email);

}