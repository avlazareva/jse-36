package ru.t1.lazareva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );


}