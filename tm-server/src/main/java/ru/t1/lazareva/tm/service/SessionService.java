package ru.t1.lazareva.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.repository.ISessionRepository;
import ru.t1.lazareva.tm.api.service.ISessionService;
import ru.t1.lazareva.tm.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }

}