package ru.t1.lazareva.tm.repository;

import ru.t1.lazareva.tm.api.repository.ISessionRepository;
import ru.t1.lazareva.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}